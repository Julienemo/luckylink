Rails.application.routes.draw do

  mount RailsAdmin::Engine => '/admin', as: 'rails_admin'
  devise_for :users
  root "static#home"

  get '/proposez_votre_luli', to: 'static#association_invite'

  get '/presentation', to:'static#presentation'


  resources :static, only: [:home]
  resources :users, except: [:index, :new, :create]
  resources :contacts, only: [:new, :create], path: 'contact'
  resources :lulis do
    resources :bids,except: [:index, :edit, :destroy]
  end

end
