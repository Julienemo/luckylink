require 'test_helper'

class PageOpenTest < ActionDispatch::IntegrationTest
  test "anyone should be able to access home page" do
    get "/"
    assert_response :success
    # initially tried to put such test in model test
    # didn't work
    # obviously it needs to be in the integration test
  end

end
