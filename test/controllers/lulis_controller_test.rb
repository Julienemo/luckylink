require 'test_helper'

class LulisControllerTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers

  setup do
    @user = users(:association_bis)
    sign_in @user
    @new_luli = lulis(:new_luli)
  end

  test "should get index with a user" do
    get lulis_path
    assert_response :success
  end

  test "should get new" do
    get new_luli_path
    assert_response :success
  end

  test "should NOT get new for ENTREPRISE user" do
    sign_in(users(:entreprise_user))
    get new_luli_path
    assert_redirected_to root_path
    assert_equal "Seules les associations peuvent creer des Lulis.", flash[:notice]
  end

  test "should get show" do
    get luli_path(@new_luli)
    assert_response :success
  end

  test "should NOT get show without user" do
    sign_out(@user)
    get luli_path(@new_luli)
    assert_redirected_to new_user_session_path
    assert_equal "Vous devez vous connecter pour accéder à cette page.", flash[:notice]
  end

  test "should get edit of one's own Luli" do
    get edit_luli_path(@new_luli)
    assert_response :success
  end

  test "should NOT get edit of someone else's Luli" do
    sign_in(users(:association_user))
    get edit_luli_path(@new_luli)
    assert_redirected_to luli_path(@new_luli)
    assert_equal "Vous n'avez pas accès à cette page.", flash[:notice]
  end

  test "should create with valid params" do
    image = fixture_file_upload('files/seeds1.png', 'image/jpg')
    assert_difference("Luli.count") do
      post lulis_path, params: {active: false, admin_validate: false, name: "a different name of luli", description: "description oups description oups description oups description oups description oups description oups description oups description oups",request: "request oups request oups request oups request oups request oups", partnership: "Mécenat_de_compétences", pathology: "Maladies_rares", contact_name: "Jean-François Machin",contact_number: "0601020304", contact_email: "adressedunnouveaululi@yopmail.com", association_description: "association_description_oups association_description_oups association_description_oups association_description_oups association_description_oups association_description_oups ", comment: "just need this one", cover_photo: image}
    end

    assert_redirected_to luli_path(Luli.last)
    assert_equal "Votre Luli a été crée avec succès ! Une fois validé par l'administrateur, il sera visibile sur le site dans l'onglet 'Entreprises, trouvez votre Luli'.", flash[:success]

    @luli = Luli.last
    assert_redirected_to luli_path(@luli)
    assert_equal false, @luli.active
    assert_equal false, @luli.admin_validate
    assert_equal "description oups description oups description oups description oups description oups description oups description oups description oups", @luli.description
    assert_equal "request oups request oups request oups request oups request oups", @luli.request
    assert_equal "Mécenat_de_compétences", @luli.partnership
    assert_equal "Maladies_rares", @luli.pathology
    assert_equal "Jean-François Machin", @luli.contact_name
    assert_equal "0601020304", @luli.contact_number
    assert_equal "adressedunnouveaululi@yopmail.com", @luli.contact_email
    assert_equal "association_description_oups association_description_oups association_description_oups association_description_oups association_description_oups association_description_oups ", @luli.association_description
    assert_equal "just need this one", @luli.comment

    ###########################
    ##ATTACHMENT TO BE TESTED##
    ###########################

    admin_mail = ActionMailer::Base.deliveries.first
    broker_mail = ActionMailer::Base.deliveries.last

    assert_equal admin_mail.to, ["jlquatorze@yopmail.com", "jbpoqueline@yopmail.com"]
    assert_equal broker_mail.to, ["jfhoussa@yopmail.com"]

    assert_equal admin_mail.subject, "[LuckyLink] Nouveau Luli poste"
    assert_equal broker_mail.subject, "[LuckyLink] Vous venez de publier un nouveau Luli"
  end

  test "should not create without photo" do
    assert_no_difference("Luli.count") do
      post lulis_path, params: {active: false, admin_validate: false, name: "a different name of luli", description: "description oups description oups description oups description oups description oups description oups description oups description oups",request: "request oups request oups request oups request oups request oups", partnership: "Mécenat_de_compétences", pathology: "Maladies_rares", contact_name: "Jean-François Machin",contact_number: "0601020304", contact_email: "adressedunnouveaululi@yopmail.com", association_description: "association_description_oups association_description_oups association_description_oups association_description_oups association_description_oups association_description_oups ", comment: "just need this one"}
    end

    assert_match %r{Veuillez télécharger une photo de couverture pour votre Luli.}, response.body
  end

  test "should NOT create with INvalid params" do
    assert_no_difference("Luli.count") do
      post lulis_path, params: {active: false, admin_validate: false}
    end
    assert_match %r{Avez vous rempli tous les champs?}, response.body
  end

  test "should update with valid params" do
    put luli_path(@new_luli, contact_email: "unenouvelleadresse@yopmail.com")
    assert_equal @new_luli.reload.contact_email, "unenouvelleadresse@yopmail.com"
    assert_redirected_to luli_path(@new_luli.id)
    assert_equal "Modifications reussies.", flash[:success]

    admin_mail = ActionMailer::Base.deliveries.first
    broker_mail = ActionMailer::Base.deliveries.last

    assert_equal admin_mail.to, ["jlquatorze@yopmail.com", "jbpoqueline@yopmail.com"]
    assert_equal broker_mail.to, ["jfhoussa@yopmail.com"]

    assert_equal admin_mail.subject, "[LuckyLink] Un Luli a été modifié"
    assert_equal broker_mail.subject, "[LuckyLink] Un de vos Luli a été modifié"
  end

  test "should NOT update with INvalid params" do
    put luli_path(@new_luli, contact_email: "unenouvelleadresse@yopmailcom")
    assert_match %r{Veuillez réessayer.}, response.body
  end

  test "admin should be able to update" do
    sign_in(users(:admin_user))
    put luli_path(@new_luli, request: "nouvelle formulation de besoin proposée par l'admin", admin_validate: true)
    assert_redirected_to luli_path(@new_luli)

    assert_equal @new_luli.reload.admin_validate, true
    assert_equal @new_luli.reload.active, true
  end

  test "should delete" do
    assert_difference("Luli.count", -1) do
      delete luli_path(@new_luli)
    end
    assert_redirected_to root_path
    assert_equal "Ce Luli a bien été supprimé.", flash[:success]
    assert_nil Luli.find_by(broker: @user, name: @new_luli.name)
  end

end
