require 'test_helper'

class ContactsControllerTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers

  test "should get new" do
    get new_contact_path
    assert_response :success
  end

  test "creates a contact record and sends an email" do
    sign_in(users(:association_bis))
    assert_difference('Contact.count') do
      post contacts_path, params: { user_id: users(:association_bis).id, name: "Guy's name from inside", email: "guysemailfrominside@yopmail.com", subject: "Guy says something", message: "blablablablablabla"}
    end

    assert_equal "Votre demande a bien été prise en compte. Une copie de ce message vous sera envoyée par mail.", flash[:success]


    @contact = Contact.last
    assert_equal @contact.user_id, users(:association_bis).id
    assert_equal @contact.name, "Guy's name from inside"
    assert_equal @contact.email, "guysemailfrominside@yopmail.com"
    assert_equal @contact.subject, "Guy says something"
    assert_equal @contact.message, "blablablablablabla"

    user_mail = ActionMailer::Base.deliveries.last
    assert_equal user_mail.to, [@contact.email] # there might be different recipients, hence the array
    assert_equal user_mail.subject, "[LuckyLink] vous venez de nous ecrire"

    @admin_user = users(:admin_user)
    @admin_bis = users(:admin_bis)
    admin_mail = ActionMailer::Base.deliveries.first
    assert_equal admin_mail.to, [@admin_user.email, @admin_bis.email]
    assert_equal admin_mail.subject, "[LuckyLink] demande de contact"
  end

  test "doesn't create contact when at least one of the blanks are not properly filled" do
    assert_no_difference('Contact.count') do
      post contacts_path, params: { email: "guysemailfrominside@yopmail.com", subject: "Guy says something", message: "blablablablablabla"}
    end

    assert_match %r{pas pu enregistrer votre demande. Veuillez renseigner tous les champs.}, response.body
  end
end
