require 'test_helper'

class UsersControllerTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers
  # devise allows the sign_in

  setup do
    @user = users(:entreprise_user)
    sign_in @user
  end

  test "should be able to see profile" do
    get user_path(@user)
    assert_response :success
  end

  test "returns no error when pass random id param" do
    get user_path("asfadsfa")
    assert_response :success
  end

  test "should be able to edit profile" do
    get edit_user_path(@user)
    assert_response :success
  end

  test "should update profile with valid information" do
    put user_path(@user, first_name: 'aloha')
    assert_equal @user.reload.first_name, "aloha"
    assert_redirected_to user_path(@user.id)
    # user_path(@user) and user_path(@user.id) not recognized as the same url
    # user_path(@user) => users/friendly_id_slug
    # user_path(@user_id) => users/q98345983875045 etc

    assert_equal "Vos modifications ont bien été prises en compte.", flash[:success]
    # to test flashes in redirection
  end

  test "should NOT update profile with INvalid information" do
    put user_path(@user, first_name: '')
    assert_response :success
    assert_not_equal @user.reload.first_name, ''
    # @user.first_name will always return the value passed in the put params
    # BUT, the app will understand it's not a valid value and not save
    # so the reloaded value is the crrently saved value of the instantce

    assert_match %r{La mise à jour de votre profil à échoué. Avez-vous rempli tous les champs ?}, response.body
    # to test flashes in render
  end

  test "should be able to delete profile" do
    assert_difference('User.count', -1) do
      delete user_path(@user)
    end
    assert_response :success
    assert_nil User.find_by(email: @user.email)
  end
end
