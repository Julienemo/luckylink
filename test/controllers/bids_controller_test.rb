require 'test_helper'

class BidsControllerTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers

  setup do
    @bidder = users(:entreprise_bis)
    @broker = users(:association_user)
    @luli = lulis(:active_luli)
    sign_in @bidder
  end

  test "should get show if logged in as bidder" do
    @bid = bids(:bid_bis)
    get luli_bid_path(@luli, @bid)
    assert_response :success
  end

  test "should get show if logged in as broker" do
    sign_in(users(:association_user))
    @bid = bids(:bid_bis)
    get luli_bid_path(@luli, @bid)
    assert_response :success
  end

  test "should NOT get show if logged as another enterprise user" do
    sign_in(users(:entreprise_user))
    @bid = bids(:bid_bis)
    get luli_bid_path(@luli, @bid)
    assert_redirected_to luli_path(@bid.luli)
    assert_equal "Vous n'avez pas accès à cette information.", flash[:notice]
  end

  test "should get new for a non-bid luli" do
    get new_luli_bid_path(@luli)
    assert_response :success
  end

  test "should NOT get new if logged in as association" do
    sign_in(users(:association_user))
    get new_luli_bid_path(@luli)
    assert_redirected_to root_path
    assert_equal "Vous pouvez contacter l'association directement.", flash[:success]
  end

  test "should NOT create with INvalid input" do
    sign_in (users(:entreprise_ter))
    assert_no_difference("Bid.count") do
      post luli_bids_path(@luli), params:{
      contact_name: "nooooofromtest",
      contact_number: "0405050505",
      contact_email: "riennestvrai@yopmail",
      contact_role: "stagiaire",
      motivation: "motivation alors motivation alors motivation alors motivation alors motivation alors",
      argument: "argument alors argument alors argument alors argument alors argument alors ",
      comment: "comment alors comment alors comment alors comment alors "
      }
    end
    assert_match %r{Demande non enregistrée.}, response.body
  end

  test "should create with valid input" do
    @user = users(:entreprise_ter)
    sign_in (@user)
    assert_difference("Bid.count") do
      post luli_bids_path(@luli), params:{
      contact_name: "nooooofromtest",
      contact_number: "0405050505",
      contact_email: "riennestvrai@yopmail.com",
      contact_role: "stagiaire",
      motivation: "motivation alors motivation alors motivation alors motivation alors motivation alors",
      argument: "argument alors argument alors argument alors argument alors argument alors ",
      comment: "comment alors comment alors comment alors comment alors "
      }
    end
    assert_equal "La demande de mise en relation a bien été prise en compte.", flash[:success]

    @bid = Bid.last
    assert_equal @bid.bidder, @user
    assert_equal @bid.luli, @luli
    assert_equal @bid.contact_name, "nooooofromtest"
    assert_equal @bid.contact_number, "0405050505"
    assert_equal @bid.contact_email, "riennestvrai@yopmail.com"
    assert_equal @bid.contact_role,"stagiaire"
    assert_equal @bid.motivation, "motivation alors motivation alors motivation alors motivation alors motivation alors"
    assert_equal @bid.argument, "argument alors argument alors argument alors argument alors argument alors "
    assert_equal @bid.comment, "comment alors comment alors comment alors comment alors "
    assert_equal @bid.pending, true
    assert_equal @bid.accepted, false
    assert_equal @bid.refused, false


    broker_mail = ActionMailer::Base.deliveries[0]
    bidder_mail = ActionMailer::Base.deliveries[1]

    assert_equal broker_mail.to, [@luli.broker.email]
    assert_equal bidder_mail.to, [@bid.bidder.email]

    assert_equal broker_mail.subject, "[LuckyLink] Votre Luli intéresse"
    assert_equal bidder_mail.subject, "[LuckyLink] Votre demande de mise en contact"
  end

  test "should update if logged in as broker" do
    sign_in(@broker)
    @bid = bids(:bid_bis)
    put luli_bid_path(@luli, @bid, status: "accepted")
    assert_equal @bid.reload.accepted, true
    assert_equal "Félicitations ! Votre luli a abouti à un partenariat ! Celui-ci sera désormais visible dans la liste des lulis archivés.", flash[:success]

  end

  ########################
  ###MAILER TO BE TESTED##
  ########################

end
