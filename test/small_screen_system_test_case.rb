require "test_helper"

class SmallScreenSystemTestCase < ActionDispatch::SystemTestCase
  Capybara.register_driver(:small_window_chrome) do |app|
    options = Selenium::WebDriver::Chrome::Service::Options.new
    options.add_argument("window-size=500,500")
    options.add_argument("headless")
    options.add_argument("disable-gpu")

    Capybara::Selenium::Driver.new(
      app,
      browser: :chrome,
      options: options
    )
  end

  driven_by :small_window_chrome
end
