require "test_helper"

class ApplicationSystemTestCase < ActionDispatch::SystemTestCase
  Capybara.register_driver(:big_window_chrome) do |app|
    # :big_window_chrome is just a variable name
    options = Selenium::WebDriver::Chrome::Serivce::Options.new
    options.add_argument("window-size=1400,1400")
    options.add_argument("headless")
    options.add_argument("disable-gpu")

  Capybara::Selenium::Driver.new(
    app,
    browser: :chrome,
    options: options
    )
  end

  driven_by :big_window_chrome
end

# by default, a system test does exactly what a humain does
# it opens an explorer, goes to a website, clicks on a link, fills in a blank etc
# by default, at each test "xxx" do of a system test
# an explorer is physically open, which has the same display as user's default set
# which means it might launch a "mobile devise" explorer if it is the user's set

# the option "headless" does not opens physically the explorer
# the screen_size explicitly set the screen to a size

# it is also possible to test the responsiveness on different screen sizes
# another SystemTestCase needs to be defined with another screen_size
# the correspondant tests should inherent the other SystemTestCase with a different screen size

# in this application, there is a SmallScreenSystemTestCase
