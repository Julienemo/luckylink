require "application_system_test_case"

class HomePageHappyPathTest < ApplicationSystemTestCase
  # these tests happen on a 1400 x 1400 screen, which is a "PC" size

  test "0-no toggler on big screen" do
    visit root_path
    assert_raises(Capybara::ElementNotFound) do
      click_on "the_toggler"
    end
  end

  test "1-test home page association top button" do
    visit root_path
    assert_selector "h1", text: "Entreprise, Association"
    click_on "association_home_top"
    assert_selector "h2", text:"Connexion" # flash is not tested
  end

  test "2-test home page entreprise top button" do
    visit root_path
    assert_selector "h1", text: "Entreprise, Association"
    click_on "entreprise_home_top"
    assert_selector "h3", text: "Les projets des associations en recherche d'un partenaire industriel"
  end

  test "3-test home page association middle button" do
    visit root_path
    assert_selector "h1", text: "Entreprise, Association"
    click_on "association_home_middle"
    assert_selector "h2", text:"Connexion" # flash is not tested
  end

  test "4-test home page entreprise middle button" do
    visit root_path
    assert_selector "h1", text: "Entreprise, Association"
    click_on "entreprise_home_middle"
    assert_selector "h3", text: "Les projets des associations en recherche d'un partenaire industriel"

    # tourist can't see details of luli
    click_on(class: 'see_luli')
    assert_selector "h2", text:"Connexion" # flash is not tested
    end

  test "5-test home page what is middle button" do
    visit root_path
    assert_selector "h1", text: "Entreprise, Association"
    click_on "what_is_home_middle"
    assert_selector "h1", text:"QU'EST CE QU'UN LULI ?"
  end

  test "6-test navbar 'qu'est ce que" do
    visit root_path
    click_on "what_is_nav"
    assert_selector "h1", text:"QU'EST CE QU'UN LULI ?"
  end

  test "7-test navbar 'trouvez luli" do
    visit root_path
    click_on "lulis_nav"
    assert_selector "h3", text: "Les projets des associations en recherche d'un partenaire industriel"
  end

  test "8-test navbar 'proposez luli" do
    visit root_path
    click_on "new_luli_nav_tourist"
    assert_selector "h1", text:"Association, proposez un Luli"
    click_on "connexion_asso_invit"
    assert_selector "h2", text: "Connexion"
  end

  test "9-test navbar 'contactez nous" do
    visit root_path
    click_on "contact_nav"
    assert_selector "h2", text:"Contactez-nous"
  end

  test "10-test navbar, inscription, connexion, logo and footer back_home" do
    visit root_path
    click_on "inscription_nav"
    assert_selector "h2", text:"Inscription"
    click_on "connexion_shared"
    assert_selector "h2", text:"Connexion"
    click_on "inscription_shared"
    assert_selector "h2", text: "Inscription"
    click_on "logo_nav"
    assert_selector "h2", text: "Lucky Link fait le lien entre les acteurs de la santé"
    click_on "connexion_nav"
    assert_selector "h2", text:"Connexion"
    click_on "forgotten_shared"
    assert_selector "h2", text: "Mot de passe oublié"
    click_on "home_footer"
    assert_selector "h2", text: "Lucky Link fait le lien entre les acteurs de la santé"
  end


  test "11-test footer contact" do
    visit root_path
    click_on "contact_footer"
    assert_selector "h2", text:"Contactez-nous"
  end

  test "12-test footer what is" do
    visit root_path
    click_on "what_is_footer"
    assert_selector "h1", text:"QU'EST CE QU'UN LULI ?"
  end

  test "13-create a new contact as a tourist in both pages" do
    # needs to comment out the after_creation: after_create :new_contact_notification
    # in contact model for this to work
    # I think the problem comes from the fact there is no admin yet to receive the mail
    visit root_path
    fill_in "home_tourist_name", with: "Guy's name H"
    fill_in "home_tourist_email", with: "guysemail@yopmail.com"
    fill_in "home_subject", with: "Guy's suject"
    fill_in "home_message", with: "Guy says blablablablablabla"
    assert_difference('Contact.count') do
      click_on "home_contact"
    end
    click_on "contact_nav"
    fill_in "contact_tourist_name", with: "Guy's name C"
    fill_in "contact_tourist_email", with: "otherguysemail@yopmail.com"
    fill_in "contact_subject", with: "other Guy's suject"
    fill_in "contact_message", with: "other Guy says blablablablablabla"
    assert_difference('Contact.count') do
      click_on "contact_contact"
    end
  end
end
