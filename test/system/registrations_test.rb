require "application_system_test_case"

class RegistrationsTest < ApplicationSystemTestCase
  include Devise::Test::IntegrationHelpers

  test "register through website and send mails" do
    visit new_user_registration_path
    assert_selector "h2", text: "Inscription"
    choose("radio_association")
    fill_in "registrer_org_name", with: "Association française de testeurs d'Applications-1001 testeurs"
    fill_in "registrer_role", with: "testeuseéé"
    fill_in "registrer_lname", with: "TTT"
    fill_in "registrer_fname", with: "Ttt"
    fill_in "registrer_email", with: "unefausseasso@yopmail.com"
    fill_in "registrer_pw", with: "1111111"
    fill_in "registrer_confirm_pw", with: "1111111"
    click_on "registrer_submit"

    @user = User.last
    assert_equal @user.first_name, "Ttt"
    assert_equal @user.last_name, "TTT"
    assert_equal @user.org_type, "association"
    assert_equal @user.org_name, "Association française de testeurs d'Applications-1001 testeurs"
    assert_equal @user.email, "unefausseasso@yopmail.com"

    user_mail = ActionMailer::Base.deliveries.last
    assert_equal user_mail.to, [@user.email]
    assert_equal user_mail.subject, "[LuckyLink] Bienvenue chez nous !"

    @admin_user = users(:admin_user)
    @admin_bis = users(:admin_bis)
    admin_mail = ActionMailer::Base.deliveries.first
    assert_equal admin_mail.to, [@admin_user.email, @admin_bis.email]
    assert_equal admin_mail.subject, "[LuckyLink] Nouvel utilisateur"

    # since tests don't mess with the real database
    # within this test there are only two mails deliveries
    # (ActionMailer::Base.deliveries.count == 2)
    # the last mail is sent to the new user
    # the second last (which is the first) is sent to the admin
  end
end
