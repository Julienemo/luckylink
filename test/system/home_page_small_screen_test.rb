require "small_screen_system_test_case"

class HomePageSmallScreenTest < SmallScreenSystemTestCase
  test "0-toggler is there on small screens" do
    visit root_path

    # can't directly acces the navbar links
    assert_raise(Capybara::ElementNotFound) do
      click_on "what_is_nav"
    end

    # needs to click on the toggler to get links
    click_on "the_toggler"
    click_on "what_is_nav"
    assert_selector "h1", text:"QU'EST CE QU'UN LULI ?"
  end
end
