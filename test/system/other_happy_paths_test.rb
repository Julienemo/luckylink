require "application_system_test_case"

class OtherHappyPathsTest < ApplicationSystemTestCase
  test "1-two buttons is 'what is' page" do
    visit root_path
    click_on "what_is_nav"
    click_on "entreprise_what_is"
    assert_selector "h3", text: "Les projets des associations en recherche d'un partenaire industriel"
    click_on "logo_nav"
    click_on "what_is_nav"
    click_on "association_what_is"
    assert_selector "h2", text: "Connexion"
  end

  test "2-see luli on lulis_path" do
    visit root_path
    click_on "lulis_nav"
  end
end
