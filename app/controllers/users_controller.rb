class UsersController < ApplicationController
  before_action :get_current_user

  def show
    # it's really important that here the url not take any params
    # otherwise a person can go to other people's profile
    # the before_action makes sure whatever params is passed, the page stays at current_user profile
  end

  def edit
  end

  def update
    if !@user.update(user_attributes)
      flash[:notice] = "La mise à jour de votre profil à échoué. Avez-vous rempli tous les champs ?"
      render "edit"
    else
      flash[:success] = "Vos modifications ont bien été prises en compte."
      redirect_to user_path(@user.id)
    end
  end

  def destroy
    @user.destroy
  end

  private
  def user_attributes
    params.permit(:first_name,
    :last_name, :org_name, :role)
  end
end
