class LulisController < ApplicationController
  before_action :get_current_user, except:[:index]
  before_action :get_current_luli, except: [:index, :new, :create]
  before_action :can_create_luli, only: [:new, :create]
  before_action :can_edit_luli, only: [:edit, :update, :destroy]
  before_action :get_pathologies, only: [:new, :create, :edit, :update]
  before_action :get_partnerships, only: [:new, :create, :edit, :update]

  def index
    @active_lulis = Luli.all.where(active: true)
    @unvalidate_lulis = Luli.all.where(admin_validate: false)
    @archived_lulis = Luli.all.where(active: false, admin_validate: true)
    if @archived_lulis.length >= 3
      @cover_luli = @archived_lulis[-1]
      @sliding_lulis = @archived_lulis[0,2]
    end
  end

  def show
    no_bid_yet
  end

  def new
    is_admin
    @luli = Luli.new(contact_name: @user.name, contact_email: @user.email, broker: @user)
  end

  def create
    @luli = Luli.new(luli_attributes.merge(broker: @user))
    if @luli.invalid?
      flash[:notice] = "Votre Luli n'a pas été crée. Avez vous rempli tous les champs?"
      render :new
    elsif !cover_photo.present?
      flash[:notice] = "Veuillez télécharger une photo de couverture pour votre Luli."
      render :new
    elsif @luli.save && !saved_attachments?
        flash[:notice] = "Nous n'avons pas réussi à sauvegarder votre pièce-jointe. Veuillez ré-essayer."
        redirect_to edit_luli_path(@luli)
    else
        flash[:success] = "Votre Luli a été crée avec succès ! Une fois validé par l'administrateur, il sera visibile sur le site dans l'onglet 'Entreprises, trouvez votre Luli'."
        redirect_to luli_path(@luli)
    end
  end

  def edit
    is_admin
  end

  def update
    if @user == @luli.broker
      @luli.update(luli_attributes)
    elsif @user.admin
      @luli.update(admin_attributes)
    end

    if @luli.admin_validate
      @luli.update(active: true)
    end

    if @luli.invalid?
      flash[:notice] = "Modifications non enregistrées. Veuillez réessayer."
      render 'edit'
    elsif cover_photo.present? && !attach_photo
      flash[:notice] = "Photo non enregistrée. Veuillez ré-essayer."
      render 'edit'
    elsif attachment.present? && !attach_file
      flash[:notice] = "Pièce jointe non enregistrée. Veuillez réessayer."
      render 'edit'
    else
      flash[:success] = "Modifications reussies."
      redirect_to luli_path(@luli)
    end
  end

  def destroy
    @luli.destroy
    flash[:success] = "Ce Luli a bien été supprimé."
    redirect_to root_path
  end


  private
  def get_current_luli
    @luli = Luli.find(params[:id])
  end

  def can_edit_luli
    if @user.admin || @user == @luli.broker
      true
    else
      flash[:notice] = "Vous n'avez pas accès à cette page."
      redirect_to luli_path(@luli)
    end
  end

  def can_create_luli
    if @user.is_association
      true
    else
      flash[:notice] = "Seules les associations peuvent creer des Lulis."
      redirect_to root_path
    end
  end

  def no_bid_yet
    bid_lulis = @user.bids_offered.map{|b| b.luli}
    @no_bid_yet = !bid_lulis.include?(@luli) && @user.org_type == "entreprise"
  end

  def luli_attributes
    params.permit(:association_description, :name, :pathology, :partnership, :description, :request, :comment, :contact_name, :contact_number, :contact_email)
  end

  def admin_attributes
    params.permit(:association_description, :description, :request, :admin_validate)
  end


  def cover_photo
    params.permit(:cover_photo)[:cover_photo]
  end

  def attachment
    params.permit(:attachment)[:attachment]
  end

  def attach_photo
    @luli.cover_photo.attach(cover_photo)
    @luli.cover_photo.attached?
  end

  def attach_file
    @luli.attachment.attach(attachment)
    @luli.attachment.attached?
  end

  def saved_attachments?
    attach_photo && (attachment.blank? || attach_file)
  end

  def get_pathologies
    @pathologies = Luli::PATHOLOGY_LIST
  end

  def get_partnerships
    @partnerships = Luli::PARTNERSHIP_LIST
  end

end
