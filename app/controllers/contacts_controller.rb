class ContactsController < ApplicationController
  def new
  end

  def create
    @contact = Contact.new(contact_attributes)
    if @contact.save
      flash[:success] = "Votre demande a bien été prise en compte. Une copie de ce message vous sera envoyée par mail."
      redirect_to lulis_path
    else
      flash[:notice] = "Nous n'avons pas pu enregistrer votre demande. Veuillez renseigner tous les champs."
      render 'new'
    end
  end

  private
  def contact_attributes
    params.permit(:name,
    :email, :subject, :message, :user_id)
  end

end
