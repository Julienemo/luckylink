class ApplicationController < ActionController::Base
  before_action :configure_permitted_parameters, if: :devise_controller?
  skip_before_action :verify_authenticity_token
  RailsAdmin.config do |config|
    config.authorize_with do
      if (current_user == nil) || !(current_user.admin)
        flash[:notice]="Vous n'avez pas acces à cette partie."
        redirect_to "/"
      end
    end
  end

  protected
  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:sign_up) { |u| u.permit(:first_name, :last_name, :org_type, :org_name, :role, :email, :password,:password_confirmation)}
  end

  private
  def get_current_user
    if current_user
      @user = current_user
    else
      flash[:notice] = "Vous devez vous connecter pour accéder à cette page."
      redirect_to new_user_session_path
    end
  end

  def is_broker
    if @user == @luli.broker
      @is_broker = true
    elsif @user == @bid.luli.broker
      @is_broker = true
    else
      flash[:notice] = "Vous ne pouvez pas accéder à ce contenu."
      redirect_to luli_path(@luli.id)
    end
    @is_broker
  end

  def is_admin
    @is_admin = @user.admin
  end

end
