class BidsController < ApplicationController
  before_action :get_current_user
  before_action :get_current_luli
  before_action :get_current_bid, only: [:show, :update]
  before_action :can_offer_bid, only: [:new, :create]
  before_action :can_read_bid, only:[:show]


  def show
  end

  def new
    @bid = Bid.new(bidder: @user, luli: @luli)
  end

  def create
    @bid = Bid.new(bid_attributes.merge(bidder: @user, luli: @luli,status: "pending"))
    if @bid.save
      flash[:success] = "La demande de mise en relation a bien été prise en compte."
      redirect_to luli_bid_path(@luli.id, @bid.id)
    else
      flash[:notice] = "Demande non enregistrée. Auriez-vous rempli tous les champs ?"
      render 'new'
    end
  end

  def update
    @bid.update(status_param)
    flash.now[:success]="La modification a bien été prise en compte"

    if @bid.status == "accepted"
      @luli.update(active: false)
      flash[:success] = "Félicitations ! Votre luli a abouti à un partenariat ! Celui-ci sera désormais visible dans la liste des lulis archivés."
      redirect_to luli_path(@luli)
    end
  end

  private

  def can_read_bid
    unless (@user == @bid.bidder)||(@user == @luli.broker)||(@user.admin)
      flash[:notice] = "Vous n'avez pas accès à cette information."
      redirect_to luli_path(@bid.luli)
    end
  end

  def can_offer_bid
    unless current_user.org_type == "entreprise"
      flash[:success] = "Vous pouvez contacter l'association directement."
      redirect_to root_path
    end
  end

  def get_current_bid
    @bid = Bid.find(params[:id])
  end

  def get_current_luli
    @luli = Luli.find(params[:luli_id])
  end

  def status_param
    params.permit(:status)
  end

  def bid_attributes
    params.permit(:contact_name, :contact_role, :contact_email, :contact_number, :motivation, :argument, :comment, :request, :comment, :contact_name, :contact_number, :contact_email)
  end


end
