class Luli < ApplicationRecord

  PATHOLOGY_LIST = %w(Cardio-Vasculaire
  Dépendances
  Dermatologie
  Maladies_génétiques
  Maladies_infectieuses
  Maladies_rares
  Neurologie
  Oncologie
  Ophtalmologie
  Psychologie
  Respiratoire
  _Prévention
  _Autre)

  PARTNERSHIP_LIST = %w(Sponsoring
  Mécenat_de_compétences
  Co-construction
  A_définir)

  belongs_to :broker, foreign_key:"broker_id", class_name: "User"
  has_many :bids, dependent: :destroy
  has_many :bidders, through: :bids, foreign_key:"bidder_id", class_name: "User"
  has_one_attached :cover_photo
  has_one_attached :attachment

  validates :name, TEXT_VALIDATIONS
  validates :description, TEXT_VALIDATIONS
  validates :request, TEXT_VALIDATIONS
  validates :contact_name, FIELD_VALIDATIONS
  validates :contact_number, phone: true
  validates :contact_email, presence: true, format: { with: EMAIL_REGEX}
  validates :pathology,presence: true, inclusion: {in: PATHOLOGY_LIST}
  validates :partnership, presence: true, inclusion: {in: PARTNERSHIP_LIST}
  validates :comment, length: 1..5000
  validates :name, uniqueness: { scope: :broker_id,
      message: "Vous avez un luli au même nom, veuillez choisir un nouveau nom pour ce projet." }

  after_create :new_luli_send
  after_update :luli_update_notification

  #########instance methods here##############
  def unvalidate
    admin_validate == false
  end

  def archived
    admin_validate && (!active)
  end
  #########mailing methods here###############

  def new_luli_send
    AdminMailer.new_luli(self).deliver_now
    LuliMailer.new_luli(self).deliver_now
  end

  def luli_update_notification
    AdminMailer.luli_update(self).deliver_now
    LuliMailer.luli_update(self).deliver_now
  end
end
