class Contact < ApplicationRecord
  belongs_to :user, optional: true
  after_create :new_contact_notification
  after_create :contact_confirmation

  validates :name, FIELD_VALIDATIONS
  validates :email, presence: true, nil: false, format: { with: EMAIL_REGEX}
  validates :subject, presence: true, nil: false, length: 5..50
  validates :message, TEXT_VALIDATIONS

  def new_contact_notification
    AdminMailer.new_contact(self).deliver_now
  end

  def contact_confirmation
    ContactMailer.confirm_contact(self).deliver_now
  end
end
