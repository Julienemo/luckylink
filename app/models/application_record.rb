require 'phonelib'
Phonelib.default_country = "FR"

class ApplicationRecord < ActiveRecord::Base
  self.abstract_class = true

  FIELD_VALIDATIONS = {
    presence: true,
    length: 1..100,
  }

  TEXT_VALIDATIONS = {
    presence: true,
    length: 5..5000
  }

  EMAIL_REGEX = /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i

  PHONE_REGEX = /\A(?:\+?|\b)([0-9]+\s\.){14}\b\z/
end
