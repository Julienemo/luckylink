class Bid < ApplicationRecord
  belongs_to :bidder, foreign_key:"bidder_id", class_name: "User"
  belongs_to :luli

  validates :contact_name, FIELD_VALIDATIONS
  validates :contact_role, FIELD_VALIDATIONS
  validates :contact_email, presence: true, format: { with: EMAIL_REGEX}
  validates :contact_number, phone: true
  validates :motivation, TEXT_VALIDATIONS
  validates :argument, TEXT_VALIDATIONS
  validates :comment, length: 1..5000
  validates :luli_id, uniqueness: { scope: :bidder_id,
      message: "Vous avez une demande en cours pour ce Luli. Veuillez attendre le retour de l'association." }


  validates :status, inclusion: { in: %w(pending accepted refused),
    message: "Veuillez choisir parmi 'pending', 'accepted', 'refused'." }

  after_create :new_bid
  #########instance methods here##############
  def pending
    status == 'pending'
  end

  def accepted
    status == 'accepted'
  end

  def refused
    status == 'refused'
  end
  #########mailing methods here###############
  def new_bid
    BidMailer.new_bid_notification(self).deliver_now
    BidMailer.new_bid_confirmation(self).deliver_now
  end
end
