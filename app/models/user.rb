class User < ApplicationRecord

  extend FriendlyId
  friendly_id :name, use: :slugged


  ORG_TYPE_LIST = %w(association entreprise site_admin)

  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable

  has_many :lulis_published, foreign_key: "broker_id",class_name: "Luli"
  has_many :bids_offered, foreign_key: "bidder_id", class_name: "Bid"
  has_many :contacts

  validates :first_name, FIELD_VALIDATIONS
  validates :last_name, FIELD_VALIDATIONS
  validates :org_type, inclusion: {in: ORG_TYPE_LIST, message: "Veuillez choisir une valeur parmi 'association', 'entreprise' et 'site_admin'" }
  validates :org_name, FIELD_VALIDATIONS
  validates :role, FIELD_VALIDATIONS
  validates :email, presence: true, format: { with: EMAIL_REGEX }
  validates_confirmation_of :password

  after_create :new_user_send, :welcome_send

  #########instance methods here##############
  def name
    first_name + " " + last_name
  end

  def is_association
    org_type == 'association'
  end

  def is_entreprise
    org_type == 'entreprise'
  end
  #########mailing methods here###############
  def new_user_send
    AdminMailer.new_user(self).deliver_now
  end

  def welcome_send
    UserMailer.welcome(self).deliver_now
  end
end
