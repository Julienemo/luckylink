class UserMailer < ApplicationMailer
  default from: 'no-reply@lucky_link.fr'

  def welcome(user)
    @user = user
    @url  = 'https://luckylink-dev.herokuapp.com/'
    mail(to: @user.email, subject: '[LuckyLink] Bienvenue chez nous !')
  end
end
