class BidMailer < ApplicationMailer
  default from: 'no-reply@lucky_link.fr'

  def new_bid_notification(bid)
    @bid = bid
    @luli = @bid.luli
    mail(to: @luli.broker.email, subject: "[LuckyLink] Votre Luli intéresse")
  end

  def new_bid_confirmation(bid)
    @bid = bid
    @luli = @bid.luli
    mail(to: @bid.bidder.email, subject: "[LuckyLink] Votre demande de mise en contact")
  end
end
