class ContactMailer < ApplicationMailer
  default from: 'no-reply@lucky_link.fr'

  def confirm_contact(contact)
    @contact = contact
    mail(to: @contact.email, subject: '[LuckyLink] vous venez de nous ecrire')
  end
end
