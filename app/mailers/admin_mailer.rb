class AdminMailer < ApplicationMailer
  admins = User.where(admin:true)

  default :to => admins.all.map(&:email),
        :from => "no-reply@lucky_link.fr"

  def new_user(user)
    @user = user
    mail(subject: '[LuckyLink] Nouvel utilisateur')
  end

  def new_luli(luli)
    @luli = luli
    mail(subject: '[LuckyLink] Nouveau Luli poste')
  end

  def luli_update(luli)
    @luli = luli
    mail(subject: '[LuckyLink] Un Luli a été modifié')
  end

  def new_contact(contact)
    @contact = contact
    if @contact.user
      @user = @contact.user
    end
    mail(subject: '[LuckyLink] demande de contact')
  end
end
