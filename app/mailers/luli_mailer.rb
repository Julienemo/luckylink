class LuliMailer < ApplicationMailer
  default from: 'no-reply@lucky_link.fr'

  def new_luli(luli)
    @luli = luli
    mail(to: @luli.broker.email, subject: '[LuckyLink] Vous venez de publier un nouveau Luli')
  end


  def luli_update(luli)
    @luli = luli
    mail(to: @luli.broker.email, subject: '[LuckyLink] Un de vos Luli a été modifié')
  end


end
