# Lucky Link

_This is a duplicate from the "real" Lucky Link project, whose repo is private. This very repo has no fork link with the project in prod._
_However the links provided below are the ones of the real project in prod. To protect the real project, testing accounts are not disclosed here._

* * *
###### Recent issues

* 2019.09.06 Attention due to Gitlab CI configuration, no local tests are possible, including `rails test` and `rails server` test on local host. Normally the app is well test-covered. Any push on Gitlab will automatically trigger pipeline test (runs exactly the same tests as `rails test`). The initial dev team is looking actively for solution to enable both CI and local test.

###### Latest release
* 2019.09.06

* * *
## Project Pitch

France is far from the best performing country in public health.

This is because associations and private companies in the health sector don't traditionnally work together. There are many campaigns all around France all the time. However, for lack of collaboration, each of these compaigns are of very limited influence.  

Lucky Link wants to save France.

Lucky Link is a matching plateform for associations and companies in the public health sector to build partnership. The project holder Oriane Bismuth likes to call it a "Tinder of public health (without the swipe)".

Associations all around France can publish campaign projects on Lucky Link (or a Luli). Private firms in the public health sector seeking to work such project can "bid" to offer different kinds of support. With Lucky Link, associations and firms know what's happening where, and everybody can join the project of everybody else, and make it larger.

### Prod link
https://luckylink.herokuapp.com/

### Main userstories

* As a visitor, you have access to different pages where the notion and value proposition of "Lucky Link" are explained. You can also browse an existing list of Luli currently looking for a private partner. Three examples of Lulis that have successfully found their partner with the help of Lucky Link are given at the buttom of the page. It is also possible to leave a message to Lucky Link team without signing in. A copy of this message will be sent by mail.

* If you register as a representative of an association (as an individual working for an association but not as the associaiton) you can browse any exisiting Luli and see the details, whether or not they are published by you. You can fill in a questionnaire about your projet. Once your answers are validated by the site admin, the project will be visible by everybody. You are informed by mail if a firm want to go further with your project. Your contact information will not be revealed to the firm until you make a contact. If you explicitly "validate" a bid, your Luli will be marked as "archived" since we consider you have already found your partner.

* If you register as an individual working for a private company looking to supporting a project, once connected, you have access to details description of existing project without contact details of the project holder. You can make a contact request on projects that are of value to you and wait for the association's reply.

* The site admin has all access to all existing projects in the admin interface and in general. However, contact request details are only accessible to relevant project holder and the firm who made a request. No Luli is visible to anybody but project holder before admin validation. Admin has access to bid and bid validations only in admin interface.


### Team

* Project holder : Oriane Bismuth
* Tech mentor :[Yoann Lecuyer](https://github.com/ylecuyer)
* Front : [Mathilde Briend](https://github.com/MutmutBlop), [Armony Saliou](https://github.com/Zalachenka)
* Back : [Nicolas Rubin ](https://github.com/jmdelpo), [Julie Kwok](https://github.com/julienemo)

Team Trello:
https://trello.com/b/YnEnUJxF/lucky-link



## Tech specifications

### Gems

In dev and prod
* devise
* faker
* phonelib : for phone number validation
* rails_admin : for admin interface
* aws-sdk-s3 : Amazon storage
* friendly_id : (we're working on its real interest)
* table_print


In test
* simplecov
We are currenlty 94.62% covered!

### API
* Send grid
* Amazon

### Difficulties in dev
* view controls (who sees what when) for different types of users on the same page
* responsiveness
* understanding Heroku errors

## Next steps (for futur devs of the project)

Front:

* customer error pages for 404 etc
* more explicit error messages for all forms
* css and attachments for mails
* 100% localization in French



Back:

* more strict data validation
* refactoration of potential partials

Function:

* multi filters by pathologies, by partnership, by date, and by Luli status in luli#index
* integrate a blog function
