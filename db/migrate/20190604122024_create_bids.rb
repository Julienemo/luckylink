class CreateBids < ActiveRecord::Migration[5.2]
  def change
    create_table :bids do |t|
      t.references :bidder, index: true
      t.references :luli, index: true
      t.string :status, default: "pending"
      t.string :contact_name
      t.string :contact_number
      t.string :contact_email
      t.string :contact_role
      t.text :motivation
      t.text :argument
      t.text :comment

      t.timestamps
    end
  end
end
