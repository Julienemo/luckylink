class CreateLulis < ActiveRecord::Migration[5.2]
  def change
    create_table :lulis do |t|
      t.references :broker, index: true
      t.boolean :active, default: true
      t.string :name
      t.text :description
      t.text :request
      t.string :partnership
      t.string :pathology
      t.string :contact_name
      t.string :contact_number
      t.string :contact_email
      t.text :association_description
      t.text :comment

      t.timestamps
    end
  end
end
