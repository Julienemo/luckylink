class CreateUsers < ActiveRecord::Migration[5.2]
  def change
    create_table :users do |t|
      t.string :first_name
      t.string :last_name
      t.boolean :admin, default: false
      t.string :org_type
      t.string :org_name
      t.string :role

      t.timestamps
    end
  end
end
