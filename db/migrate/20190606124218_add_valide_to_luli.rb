class AddValideToLuli < ActiveRecord::Migration[5.2]
  def change
    add_column :lulis, :admin_validate, :boolean, default: false, nil: false
  end
end
