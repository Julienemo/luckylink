class ChangeColumnInLuli < ActiveRecord::Migration[5.2]
  def change
    change_column_default :lulis, :active, from: true, to: false
  end
end
