first_names = ['Alice', 'Boris', 'Carlos', 'Denise', 'Elise', 'Francis']
last_names = ['Akermann', 'Brightman', 'Carlsberg', 'Derling', 'Einsenberg', 'Frisberg']
association_names = ['LES FEUX FOLLETS',
  'ASSOCIATION FRANCAISE POUR LA PREVENTION DES ALLERGIES',
  "ASSOCIATION D'AIDE AUX VICTIMES DES ACCIDENTS DES MEDICAMENTS",
  "FEDERATION NATIONALE DES ASSOCIATIONS D'USAGERS EN PSYCHIATRIE",
  "France Assos Santé",'Association Française de Promotion de la Santé Scolaire et Universitaire',
  'Fédération nationale des collèges de gynécologie médicale',
  'Syndicat National des gynécologues obstétriciens français (SYNGOF)',
  'UFC que choisir','CESPHARM',"L’Annuaire des Associations de Santé"]
partnerships = ["Sponsoring", "Mécenat_de_compétences", "Co-construction", "A_définir"]
pathologies = ["Cardio-Vasculaire", "Dépendances", "Dermatologie" ,"Maladies_génétiques", "Maladies_infectieuses", "Maladies_rares", "Neurologie", "Oncologie", "Ophtalmologie", "Psychologie", "Respiratoire", "_Prévention", "_Autre"]

puts "#"*50
puts "Clearing data history..."
Bid.destroy_all
Luli.destroy_all
User.destroy_all
puts "Data history cleared."

User.create(first_name: "Oriane", last_name: "Bismuth", admin: true, org_type: "site_admin", org_name: "luli", role: "admin", email: "jesuisadmin@yopmail.com", password: "111111")

puts "#{User.all.length} admin created."

i = 0
7.times do
  User.create(first_name: first_names.sample, last_name: last_names.sample, admin: false, org_type: "association", org_name: association_names.sample, role:  Faker::Job.position, email: "asso#{i}@yopmail.com", password: "111111")
  User.create(first_name: first_names.sample, last_name: last_names.sample, admin: false, org_type: "entreprise", org_name: Faker::Company.name, role:  Faker::Job.position, email: "boite#{i}@yopmail.com", password: "111111")
  i+= 1
end
puts "#{User.where(org_type: "association").length} assos users and #{User.where(org_type: "entreprise").length} entreprise users generated."

users = User.all
users.each do |u|
  i = 0
  if u.org_type == "association"
    3.times do
      l = Luli.create!(broker_id: u.id,
        name: "Tous avec #{u.first_name} #{i}",
        description: Faker::Lorem.paragraph_by_chars(2000, false),
        request: Faker::Lorem.paragraph_by_chars(2000, false),
        partnership: partnerships.sample,
        pathology: pathologies.sample,
        contact_name: "#{first_names.sample} #{last_names.sample}",
        contact_number: "023456789#{i}",
        contact_email: Faker::Internet.email,
        comment: Faker::Lorem.paragraph_by_chars(2000, false),
        association_description: Faker::Lorem.paragraph_by_chars(1000, false))
      n = rand(1..10)
      l.cover_photo.attach(io: File.open("app/assets/images/seeds/seeds#{n}.png"),filename: 'luli_cover.jpg')
      o = rand(1..2)
      if o == 1
        l.attachment.attach(io: File.open("app/assets/images/seeds/placeholder.pdf"),filename: 'luli_detail.pdf')
      end
      i+= 1
    end
  end
end
puts "#{Luli.all.length} luli's generated"

20.times do
  Luli.all.sample.update(admin_validate: true, active: true)
end
active_lilis = Luli.where(admin_validate: true, active: true)
puts "randomly validated #{active_lilis.length} Lulis."

lulis = Luli.all
users.each do |u|
  if u.org_type == "entreprise"
    active_lilis.each do |l|
      Bid.create!(bidder_id: u.id, luli_id: l.id,
        status: "pending",
        contact_name: "#{first_names.sample} #{last_names.sample}",
        contact_number: "023454985#{i}",
        contact_email: Faker::Internet.email,
        contact_role: Faker::Job.position,
        motivation: Faker::Lorem.paragraph_by_chars(2000, false),
        argument: Faker::Lorem.paragraph_by_chars(2000, false),
        comment: Faker::Lorem.paragraph_by_chars(2000, false)
      )
    end
  end
end
puts "#{Bid.all.length} bids's generated"

bids = Bid.all
10.times do
  b = bids.sample
  b.update(status: "accepted")
  b.luli.update(active: false)
end
puts "randomly accepted #{bids.where(status: "accepted").length} bids"
puts "#{Luli.where(admin_validate: true, active: false).length} Lulis archived."
puts "End of seeds"
